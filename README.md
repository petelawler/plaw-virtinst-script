virtinst script
================

Debian

```
debian-virtinst-preseed.sh [options] NAME RELEASE
Options:
  -h|-?         : This help
  -a ARCH       : VM architecture: x86_64, i386 (default: amd64)
  -c NUM_CPU    : VM number of CPU (default: 2)
  -m MEMORY     : VM memory size [MB] (default: 2048)
  -d DISKPOOL   : QEMU disk pool (default: default)
  -f DISKFORMAT : QEMU image format: qcow2, raw (default: qcow2)
  -s DISKSIZE   : QEMU image size, e.g., 50G (default: 8)
  -g            : Install graphical console hardware (--graphics=spice)"
  -u USERNAME   : Username of the default user (default: debian)
  -p PASSWORD   : Password for the default user (default: temppwd)
  -P            : Do not use preseed.cfg
  -k DIRNAME    : Keep preseed.cfg to DIRNAME (default: ~ )
  -e STRING     : Preseed early command (after reading preseed) STRING
  -l STRING     : Preseed late command (before reboot) STRING
  -r STRING     : Run scripts
  -y            : Dry run
  -v            : Verbose output
```

* RELEASE should be a Debian release name (e.g., jessie, stretch)
