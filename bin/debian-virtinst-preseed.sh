#!/bin/bash -e

#MIT License

#Copyright (c) 2014 Akihiro Motoki
#Copyright (c) Peter Lawler <relwalretep@gmail.com>
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# forked from
# https://github.com/amotoki/virtinst-scripts/blob/master/ubuntu-virtinst-preseed.sh

WORKDIR=$(dirname "${0}")
CONFIG_FILE=${WORKDIR}/config.sh
if [ -f "${CONFIG_FILE}" ]; then
    # shellcheck source=/dev/null
    source "${CONFIG_FILE}"
fi

SUPPORTED="(stretch|buster)"

ARCH=amd64

SITE=${DEBIAN_SITE:-http://deb.debian.org}
PROXY=${DEBIAN_PROXY:-${PROXY:-$http_proxy}}
ISO_LOCATION_DIR=${DEBIAN_ISO_DIR:-$HOME/Downloads/iso/debian}
# /etc/systemd/timesyncd.conf
NTPSERVER=${NTPSERVER:-0.debian.pool.ntp.org}
USERNAME=debian
PASSWORD=temppwd
NUM_CPU=2
MEMORY=2048
DISKSIZE=8
DISKFORMAT=qcow2
DISKPOOL=default
SPARSE=yes
GRAPHICS=none
WITH_GRAPHICS="spice"
WITH_VIDEO="--video qxl --channel spicevmc"
#accel3d disabled for now as buster doesn't?
#WITH_VIDEO="--video virtio,accel3d=yes"
SAVE_PRESEEDDIR="${HOME}"
EARLYCOMMAND=""
LATECOMMAND=""
CONSOLE=console=ttyS0,115200
AUTOCONSOLE=""
DEFAULT_LATECOMMAND="in-target sh -c 'echo GRUB_TERMINAL=serial console | tee -a /etc/default/grub.d/grub-terminal'"
PRESEED_DIR=${PRESEED_DIR:-/tmp/preseed$$}
PRESEED_BASENAME=preseed.cfg
PRESEED_FILE=${PRESEED_DIR}/${PRESEED_BASENAME}

function sudo_check() {
    # priv check
    echo "Checking for sudo"
    if [ "$(id | grep root )" ]; then
        echo "Probably shouldn't be run as root… but proceeding anyway"
    elif [ "$(sudo -v)" ]; then
            echo "sudo privileges needed, exiting"
            exit
    fi
echo "sudo access available"
}

function usage() {
    cat <<EOF
Usage: ${0} [options] NAME RELEASE

Options:
  -h|-?         : This help
  -a ARCH       : VM architecture: amd64, i386 (default: ${ARCH})
  -c NUM_CPU    : VM number of CPU (default: ${NUM_CPU})
  -m MEMORY     : VM memory size [MB] (default: ${MEMORY})
  -d DISKPOOL   : QEMU disk pool (default: ${DISKPOOL})
  -f DISKFORMAT : QEMU image format: qcow2, raw (default: ${DISKFORMAT})
  -s DISKSIZE   : QEMU image size, e.g., 50G (default: ${DISKSIZE})
  -g            : Install graphical console hardware (--graphics=${WITH_GRAPHICS})
  -u USERNAME   : Username of the default user (default: ${USERNAME})
  -p PASSWORD   : Password for the default user (default: ${PASSWORD})
  -P            : Do not use preseed.cfg
  -k DIRNAME    : Keep preseed.cfg to DIRNAME (default: ${SAVE_PRESEEDDIR})
  -e STRING     : Preseed early command (after reading preseed) STRING
  -l STRING     : Preseed late command (before reboot) STRING
  -r STRING     : Run scripts
  -n            : No autoconsole
  -t STRING     : .iso type (dvd|netinst)
  -w STRING     : Network name
  -y            : Dry run
  -v            : Verbose output

Environment variables:
  DISKIMG_DIR=${DISKIMG_DIR}
  DEBIAN_SITE=${SITE}
  DEBIAN_PROXY=${PROXY}
  DEBIAN_ISO_DIR=${ISO_LOCATION_DIR}
  DEBIAN_ISO_LOCATION_FORMAT=${ISO_LOCATION_FORMAT}
  DEBIAN_ISO_LOCATION_TYPE=${ISO_LOCATION_TYPE}
  PRESEED_DIR=${PRESEED_DIR}
EOF
    exit 1
}

while getopts "a:c:m:f:s:u:p:e:l:r:t:w:Pnkgdhyv" OPT; do
    case ${OPT} in
        a) ARCH=${OPTARG}
           if [ "${ARCH}" != "i386" ] && [ "${ARCH}" != "amd64" ]; then
                echo "Arch must be either amd64 or i386."
                exit 1
           fi
           ;;
        c) NUM_CPU=${OPTARG}; ;;
        m) MEMORY=${OPTARG}; ;;
        f) DISKFORMAT=${OPTARG}
           if [ "${DISKFORMAT}" != "qcow2" ] && [ "${DISKFORMAT}" != "raw" ]; then
                echo "Disk format must be either qcow2 or raw."
                exit 1
           fi
           ;;
        n) NOAUTOCONSOLE="--noautoconsole"; ;;
        t) TYPE=${OPTARG}
           if [ "${TYPE}" != "dvd" ] && [ "${TYPE}" != "netinst" ]; then
               echo "-t TYPE dvd|netinst"
               exit 1
           fi
           ;;
        s) DISKSIZE=${OPTARG}; ;;
        d) DISKPOOL=${OPTARG}; ;;
        u) USERNAME=${OPTARG}; ;;
        p) PASSWORD=${OPTARG}; ;;
        P) NO_PRESEED=true; ;;
        k) SAVE_PRESEEDDIR=${OPTARG}; ;;
        e) EARLYCOMMAND=${OPTARG}; ;;
        l) LATECOMMAND=${OPTARG}; ;;
        g) GRAPHICS=${WITH_GRAPHICS}; VIDEO=${WITH_VIDEO};
            unset CONSOLE ;;
        w) NETWORK_NAME=${OPTARG}; ;;
        r) RUNSCRIPTS=${OPTARG}; ;;
        y) DRYRUN=true; ;;
        v) VERBOSE=true; VERBOSE_SWITCH="-v";;
        h) usage ;;
        ?) usage ;;
    esac
done
shift $((OPTIND - 1))

if [ "${VERBOSE}" ]; then
    echo "ARCH=${ARCH}"
    echo "SITE=${SITE}"
    echo "PROXY=${PROXY}"
    echo "DISKIMG_DIR=${DISKIMG_DIR}"
    echo "ISO_LOCATION_DIR=${ISO_LOCATION_DIR}"
    echo "NTPSERVER=${NTPSERVER}"
    echo "USERNAME=${USERNAME}"
    echo "PASSWORD=${PASSWORD}"
    echo "NUM_CPU=${NUM_CPU}"
    echo "MEMORY=${MEMORY}"
    echo "DISKSIZE=${DISKSIZE}"
    echo "DISKFORMAT=${DISKFORMAT}"
    echo "DISKPOOL=${DISKPOOL}"
    echo "NETWORK_NAME=${NETWORK_NAME}"
    echo "EARLYCOMMAND=${EARLYCOMMAND}"
    echo "LATECOMMAND=${LATECOMMAND}"
    echo "RUNSCRIPTS=${RUNSCRIPTS}"
    echo "NOAUTOCONSOLE=${NOAUTOCONSOLE}"
    echo "DRYRUN=${DRYRUN}"
    echo "SPARSE=${SPARSE}"
fi

if [ -z "${1}" ]; then
    echo "NAME must be specified!"
    echo "Usage: ${0} [options] NAME RELEASE"
    usage
    exit 1
fi

if [ -z "${2}" ]; then
    echo "RELEASE must be specified! ${SUPPORTED}"
    echo "Usage: ${0} [options] NAME RELEASE"
    exit 1
fi

NAME=${1}
if [ "${VERBOSE}" ]; then
    echo "NAME = ${NAME}"
fi
if [ "${DISKPOOL}" ]; then
    DISK="pool=${DISKPOOL},disk=${NAME}.img"
else
    DISK="disk=${DISKIMG_DIR}/${NAME}.img"
fi
if [ "${VERBOSE}" ]; then
    echo "DISK = ${DISK}"
fi

if [ -z "${PASSWORD}" ]; then
    PASSWORD=${NAME}
fi
if [ "${VERBOSE}" ]; then
    echo "PASSWORD = ${PASSWORD}"
fi

RELEASE_NAME=${2}
if [[ ! "${RELEASE_NAME}" =~ ${SUPPORTED} ]]; then
    echo "Release '${RELEASE_NAME}' is not supported."
    echo "${SUPPORTED} must be specified"
    exit 2
fi
if [ "${RELEASE_NAME}" == "oldstable" ]; then
    RELEASE_NAME="stretch"
elif [ ${RELEASE_NAME} == "bullseye" ]; then
    RELEASE_NAME="testing"
fi
if [ "${VERBOSE}" ]; then
    echo "RELEASE_NAME = ${RELEASE_NAME}"
fi

if [ "${ARCH}" == "amd64" ]; then
    VIRT_ARCH=x86_64
else
    VIRT_ARCH=i386
fi
if [ "${VERBOSE}" ]; then
    echo "VIRT_ARCH = ${VIRT_ARCH}"
fi
	
case "${RELEASE_NAME}" in
  stretch)
    RELEASE_FULLVER=9
    RELEASE_MINORVER=13.0
    ;;
  buster)
    RELEASE_FULLVER=10
    RELEASE_MINORVER=10.0
    ;;
  testing)
    RELEASE_FULLVER=testing
    ;;
esac
if [ "${VERBOSE}" ]; then
    echo "RELEASE_NAME = ${RELEASE_NAME}"
    echo "RELEASE_FULLVER = ${RELEASE_FULLVER}"
fi

if [ -z "${OS_VARIANT}" ]; then
    OS_VARIANT="debian${RELEASE_FULLVER}"
fi
if [ "${VERBOSE}" ]; then
    echo "OS_VARIANT = ${OS_VARIANT}"
fi

LOCATION=${SITE}/debian/dists/${RELEASE_NAME}/main/installer-${ARCH}/
if [ "${VERBOSE}" ]; then
    echo "LOCATION = ${LOCATION}"
fi

# You can use the following keyword
# %ISO_DIR%
# %ARCH%
# %RELEASE_NAME% : stretch...
# %RELEASE_VERSION% : 9, ...
# %RELEASE_FULLVER% : 9.9, ...

#ISO_LOCATION_FORMAT_DEFAULT=%ISO_DIR%/ubuntu-%RELEASE_FULLVER%-server-%ARCH%.iso
if [ "${TYPE}" = "dvd" ]; then
        TYPE="DVD-1"
fi
ISO_LOCATION_FORMAT=${ISO_LOCATION_DIR}/debian-%RELEASE_FULLVER%.%RELEASE_MINORVER%-%ARCH%-%TYPE%.iso
#ISO_LOCATION_FORMAT=${DEBIAN_ISO_LOCATION_FORMAT:-${ISO_LOCATION_FORMAT}_DEFAULT}
if [ "${VERBOSE}" ]; then
#    echo "ISO_LOCATION_FORMAT_DEFAULT=${ISO_LOCATION_FORMAT_DEFAULT}"
    echo "ISO_LOCATION_FORMAT=${ISO_LOCATION_FORMAT}"
fi

if [ -n "${RELEASE_FULLVER}" ]; then
    RELEASE_VERSION=$(echo "${RELEASE_FULLVER}" | cut -d . -f 1-2)
    if [ "${VERBOSE}" ]; then
        echo "RELEASE_VERSION = ${RELEASE_VERSION}"
    fi
    ISO_LOCATION=$(echo "${ISO_LOCATION_FORMAT}" | sed \
                      -e "s|%ISO_LOCATION_DIR%|${ISO_LOCATION_DIR}|g" \
                      -e "s|%ARCH%|${ARCH}|g" \
                      -e "s|%RELEASE_NAME%|${RELEASE_NAME}|g" \
                      -e "s|%RELEASE_VERSION%|$RELEASE_VERSION|g" \
                      -e "s|%RELEASE_FULLVER%|${RELEASE_FULLVER}|g" \
                      -e "s|%RELEASE_MINORVER%|${RELEASE_MINORVER}|g" \
                      )
    if [ "${VERBOSE}" ]; then
        echo "ISO_LOCATION = ${ISO_LOCATION}"
    fi
    if [ -f "${ISO_LOCATION}" ]; then
        LOCATION=${ISO_LOCATION}
    fi
    if [ "${VERBOSE}" ]; then
        echo "LOCATION = ${LOCATION}"
    fi
fi

if [ "$EARLYCOMMAND" ]; then
    EARLYCOMMAND="d-i preseed/early_command string ${EARLYCOMMAND[*]}"
    if [ "${VERBOSE}" ]; then
        echo "EARLYCOMMAND = ${EARLYCOMMAND[*]}"
    fi
fi
if [ "$LATECOMMAND" ]; then
    LATECOMMAND="d-i preseed/late_command string ${LATECOMMAND[*]}; ${DEFAULT_LATECOMMAND}"
else
    LATECOMMAND="d-i preseed/late_command string ${DEFAULT_LATECOMMAND}"
fi
if [ "${VERBOSE}" ]; then
    echo "LATECOMMAND = ${LATECOMMAND}"
fi


function generate_preseed_cfg() {
    if [ "${VERBOSE}" ]; then
        echo "PRESEED_DIR = ${PRESEED_DIR}"
        echo "PRESEED_BASENAME = ${PRESEED_BASENAME}"
        echo "PRESEED_FILE = ${PRESEED_FILE}"
    fi
    mkdir ${VERBOSE_SWITCH} -p ${PRESEED_DIR}
    if [ "${RUNSCRIPTS}" ]; then
        for f in ${RUNSCRIPTS[*]}; do
            cp "${VERBOSE_SWITCH}" "${f}" "${PRESEED_DIR}/."
        done
        RUNSCRIPTS="d-i preseed/run string ${RUNSCRIPTS}"
    fi
    if [ "${VERBOSE}" ]; then
        echo "RUNSCRIPTS = ${RUNSCRIPTS}"
    fi
    cat > ${PRESEED_FILE} <<EOF

${EARLYCOMMAND}

d-i console-setup/ask_detect boolean false

d-i debian-installer/language string en
d-i debian-installer/country string AU
d-i debian-installer/locale string en_AU.UTF-8
d-i debian-installer/add-kernel-opts string console=tty0 console=ttyS0

d-i localechooser/supported-locales multiselect en_AU.UTF-8

d-i keyboard-configuration/xkb-keymap select us

d-i netcfg/enable boolean true
d-i netcfg/choose_interface select auto
d-i netcfg/link_wait_timeout string 3
d-i netcfg/dhcp_timeout string 10
d-i netcfg/dhcpv6_timeout string 10
d-i netcfg/get_hostname string ${NAME}
d-i netcfg/get_domain string localdomain
d-i netcfg/hostname string ${NAME}
d-i netcfg/wireless_wep string

d-i hw-detect/load_firmware boolean true

d-i mirror/country string Australia
d-i mirror/http/hostname string ${SITE}
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string ${PROXY}

d-i clock-setup/utc boolean true
d-i time/zone string Australia/Tasmania
d-i clock-setup/ntp boolean true
d-i clock-setup/ntp-server string $NTPSERVER

d-i partman-auto/method string lvm
d-i partman-auto-lvm/guided_size string max
d-i partman-auto/choose_recipe select auto
d-i partman-lvm/device_remove_lvm boolean true
d-i partman-lvm/confirm boolean true
d-i partman-lvm/confirm_nooverwrite boolean true
d-i partman-md/device_remove_md boolean true
d-i partman-md/confirm boolean true
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

d-i passwd/root-login boolean false
d-i passwd/user-fullname string ${USERNAME}
d-i passwd/username string ${USERNAME}
d-i passwd/user-password password ${PASSWORD}
d-i passwd/user-password-again password ${PASSWORD}
d-i user-setup/allow-password-weak boolean true
d-i user-setup/encrypt-home boolean false

d-i apt-setup/non-free boolean true
d-i apt-setup/contrib boolean true

tasksel tasksel/first multiselect standard

d-i pkgsel/include string acpid alpine apt-listchanges atop archivemount bash-completion beep ca-certificates cpufrequtils crash curl devio debconf-utils debfoster debsecan elinks ethtool fail2ban fdupes firmware-linux-free fonts-powerline git gpw hwinfo iftop iotop kdump-tools kexec-tools libtinfo5 locales localepurge lshw makedumpfile mailutils man-db minicom ncurses-term netselect-apt network-manager openssh-server powertop powerline pwgen screen shellcheck spice-vdagent tree tzdata ufw unattended-upgrades vim vim-addon-manager wget

# needrestart doesn't like preseed (see https://github.com/liske/needrestart/issues/66 )

debsecan debsecan/report boolean true
debsecan debsecan/suite select GENERIC

kexec-tools kexec-tools/load_kexec boolean false
kexec-tools kexec-tools/use_grub_config boolean false
kdump-tools kdump-tools/use_kdump boolean true

d-i pkgsel/upgrade select full-upgrade

popularity-contest popularity-contest/participate boolean false

d-i grub-installer/only_debian boolean true
d-i grub-installer/with_other_os boolean true
d-i grub-installer/bootdev string default
#d-i grub-installer/with_other_os boolean true
#d-i grub-installer/bootdev string /dev/vda

${LATECOMMAND}

${RUNSCRIPTS}

d-i finish-install/reboot_in_progress note
#d-i debian-installer/exit/poweroff boolean true
EOF
}

function cleanup_preseed_cfg() {
    cp -p -r ${VERBOSE_SWITCH} "${PRESEED_DIR}" "${SAVE_PRESEEDDIR}/."
    rm ${VERBOSE_SWITCH} -fr "${PRESEED_DIR}"
}

function virtinst_with_preseed() {
    if [ "${VERBOSE}" ]; then
        echo "sudo virt-install --name ${NAME} --os-type linux --os-variant ${OS_VARIANT} --virt-type kvm --connect=qemu:///system --vcpus ${NUM_CPU} --ram ${MEMORY} --arch ${VIRT_ARCH} --serial pty --console pty --disk bus=virtio,format=${DISKFORMAT},pool=${DISKPOOL},size=${DISKSIZE},sparse=${SPARSE} --graphics=${GRAPHICS} ${VIDEO} --location ${LOCATION} --initrd-inject ${PRESEED_FILE} --extra-args \"console=ttyS0,115200 file=/${PRESEED_BASENAME} auto=true priority=critical interface=auto language=en country=Australia locale=en_AU.UTF-8 console-setup/layoutcode=US console-setup/ask_detect=false\" --network network=${NETWORK_NAME},model=virtio"
    fi
    if [ ! ${DRYRUN} ]; then
        sudo virt-install \
            --name "${NAME}" \
            --os-type linux \
            --os-variant "${OS_VARIANT}" \
            --virt-type kvm \
            --connect=qemu:///system \
            --vcpus "${NUM_CPU}" \
            --cpu secure=on \
            --ram "${MEMORY}" \
            --arch "${VIRT_ARCH}" \
            --serial pty \
            --console pty \
            --disk bus=virtio,format="${DISKFORMAT}",pool="${DISKPOOL}",size="${DISKSIZE}",sparse="${SPARSE}" \
            ${NOAUTOCONSOLE} \
            --graphics="${GRAPHICS}" \
            ${VIDEO} \
            --location "${LOCATION}" \
            --initrd-inject "${PRESEED_FILE}" \
            --extra-args "${CONSOLE} file=/${PRESEED_BASENAME} auto=true priority=critical interface=auto language=en country=Australia locale=en_AU.UTF-8 console-setup/layoutcode=US console-setup/ask_detect=false " \
            --network network=${NETWORK_NAME},model=virtio
    fi
}

function virtinst_without_preseed() {

    if [ "${VERBOSE}" ]; then
        # shellcheck disable=1004
        echo "sudo virt-install --name ${NAME} --os-type linux --os-variant ${OS_VARIANT} --virt-type kvm --connect=qemu:///system --vcpus ${NUM_CPU} --ram ${MEMORY} --arch ${VIRT_ARCH} --serial pty --console pty --disk bus=virtio,format=${DISKFORMAT},pool=${DISKPOOL},size=${DISKSIZE},sparse=${SPARSE} --graphics=${GRAPHICS} --video accel3d=yes,model=virtio --location ${LOCATION} --extra-args \"${CONSOLE}\" --network network=default,model=virtio"
    fi
    if [ ! ${DRYRUN} ]; then
        sudo virt-install \
            --name "${NAME}" \
            --os-type linux \
            --os-variant "${OS_VARIANT}" \
            --virt-type kvm \
            --connect=qemu:///system \
            --vcpus "${NUM_CPU}" \
            --ram "${MEMORY}" \
            --arch "${VIRT_ARCH}" \
            --serial pty \
            --console pty \
            --disk bus=virtio,format="${DISKFORMAT}",pool="${DISKPOOL}",size="${DISKSIZE}",sparse="${SPARSE}" \
            ${NOAUTOCONSOLE} \
            --graphics="${GRAPHICS}" \
            --video accel3d=yes,model="virtio" \
            --location "${LOCATION}" \
            --extra-args "console=ttyS0,115200" \
            --network network=${NETWORK_NAME},model=virtio
    fi
}

sudo_check
if [ "${NO_PRESEED}" != "true" ]; then
    generate_preseed_cfg
    virtinst_with_preseed
    echo "Running with no auto-console, will not clean up preseed files"
    if [ ! ${NOAUTOCONSOLE} ]; then
        cleanup_preseed_cfg
    fi
else
    virtinst_without_preseed
fi
# NOTE: Can't snapshot if we're not in control of console
#       as we don't know when it completes (returns from install)
if [ ! ${NOAUTOCONSOLE} ]; then
    sudo virsh snapshot-create-as --domain "${NAME}" --name post-install-"${NAME}"
fi

echo "done"
