#!/bin/bash

# debian-vericone-sysprep.sh - a script to virt-clone and virt-sysprep a machine
#
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

function sudo_check() {
    # priv check
    echo "Checking for sudo"
    if [ "$(id | grep root )" ]; then
        echo "Probably shouldn't be run as root… but proceeding anyway"
    elif [ "$(sudo -v)" ]; then
            echo "sudo privileges needed, exiting"
            exit
    fi
echo "sudo access available"
}


function usage() {
    cat <<EOF
Usage: ${0} [options] OLDDOMAIN NEWDOMAIN

Options:
  -h    : This help
  -p    : sysprep NEWDOMAIN after cloning
  -s    : startup NEWDOMAIN after cloning
  -y    : Dry run
EOF
    exit 1
}

while getopts "psy" OPT; do
    case ${OPT} in
        p) SYSPREP=true; ;;
        s) STARTUP=true; ;;
        y) DRYRUN=true; ;;
        h) usage; ;;
        ?) usage; ;;
    esac
done
shift $((OPTIND - 1))

if [ -z "${1}" ] || [ -z "${2}" ]; then
    echo "Usage: ${0} [options] OLDDOMAIN NEWDOMAIN"
    exit 1
fi

OLDDOMAIN=${1}
NEWDOMAIN=${2}

function checkrunning() {
    WAITS=3
    SLEEP=5
    SHUTDOWN=false
    for i in $(seq ${WAITS}) ; do
        DOMSTATE=$(sudo virsh list --all | grep "${1}" | awk '{print $3}')
        if [ "${DOMSTATE}" = "shut" ]; then
            echo "Domain ${1} is shut down."
            SHUTDOWN=true
            break
        else
            echo "Domain ${1} is ${DOMSTATE}, attempting to shut down"
            sudo virsh shutdown "${1}"
            sleep ${SLEEP}
        fi
    done
    if [ ${SHUTDOWN} = false ]; then
        echo "Domain ${1} didn't shut down in ${WAITS}*${SLEEP} seconds"
        exit
    fi
}


function clone() {
    if [ -z "${DRYRUN}" ]; then
	sudo virsh snapshot-create-as --domain "${OLDDOMAIN}" --name "preclone-${OLDDOMAIN}"
        echo "sudo virt-clone --name ${NEWDOMAIN} --auto-clone --original ${OLDDOMAIN}"
        sudo virt-clone --name "${NEWDOMAIN}" --auto-clone --original "${OLDDOMAIN}"
	sudo virsh snapshot-create-as --domain "${NEWDOMAIN}" --name "initialsnapshot-${NEWDOMAIN}"
    fi
}

function sysprep() {
    echo "sudo virt-sysprep --firstboot-command \"dpkg-reconfigure openssh-server\" --hostname ${NEWDOMAIN} --domain  ${NEWDOMAIN}"
    if [ -z "${DRYRUN}" ]; then
        sudo virt-sysprep --firstboot-command "dpkg-reconfigure openssh-server" --hostname $NEWDOMAIN --domain ${NEWDOMAIN}
    fi
}

function sparsify() {
# TODO: check that virt-sparsify is available
    if [ -z "${DRYRUN}" ]; then
        FILE=$(sudo virsh dumpxml "${NEWDOMAIN}" | grep file | grep source | cut -d \' -f 2)
        if [ "${FILE}" ]; then
            echo "sudo virt-sparsify --in-place ${FILE}"
            sudo virt-sparsify --in-place "${FILE}"
        else
            echo "Error running virt-sparsify on ${FILE}"
            exit
        fi
    fi
}

sudo_check
checkrunning "${OLDDOMAIN}"
if [ "$?" = 0 ]; then
    clone
fi
if [ "$?" = 0 ] && [ "${SYSPREP}" ]; then
    sysprep
fi
sparsify
if [ "${STARTUP}" ]; then
    sudo virsh start "${NEWDOMAIN}"
fi
